# Proyecto de Desarrollo: Dashboard de Ventas con Django y Plotly

**Objetivo del Proyecto:** Crear un dashboard interactivo que muestre las ventas de una compañía, analizando datos como la ubicación de los compradores por barrios y las ventas por cada mes del año. Este proyecto integrará Django para el backend, SQLite como base de datos, y Plotly junto con Bootstrap y jQuery para el frontend.

**Duración del Proyecto:** Este proyecto está diseñado para ser completado en un máximo de semana y media, con la ayuda de un video guía que se proporcionará para facilitar el aprendizaje y desarrollo; asi tambien, haga uso de todas las herramientas que tenga a su disposicion para facilitar el proceso de produccion.

## Herramientas y Tecnologías:

- Python 3
- Django
- SQLite
- Plotly
- Bootstrap
- jQuery

## Descripción del Proyecto:

### Parte 1: Preparación del Entorno de Desarrollo

1. Instalación de Python, Django y las demás herramientas necesarias.
2. Configuración inicial del proyecto Django y la base de datos SQLite.

### Parte 2: Modelado de Datos y Administración

1. Creación de modelos en Django para representar las ventas y los barrios.
2. Configuración y uso de Django Admin para gestionar los datos fácilmente.

### Parte 3: Diseño del Frontend

1. Integración de Bootstrap y jQuery para el diseño del frontend.
2. Creación de templates HTML para el dashboard, utilizando Bootstrap para el estilo.

### Parte 4: Visualización de Datos con Plotly

1. Introducción a Plotly para la creación de gráficos interactivos basados en los datos de ventas.
2. Integración de gráficos de Plotly en Django para visualizar las ventas por barrio y por mes.

### Parte 5: Finalización y Pruebas

1. Desarrollo de vistas en Django que procesen los datos y los envíen a los templates.
2. Pruebas de la aplicación para asegurar su correcto funcionamiento.

## Instrucciones de Entrega:

- **Código Fuente:** Sube el código fuente de tu proyecto a un repositorio de GitHub o Gitlab y comparte el enlace en el aula virtual.


## Evaluación:

Tu proyecto será evaluado en base a los siguientes criterios:

- **Funcionalidad:** El dashboard debe ser funcional y permitir al usuario visualizar las ventas por barrio y por mes.
- **Integración de Tecnologías:** Debes integrar efectivamente Django, SQLite, Plotly, Bootstrap y jQuery.

## Nota Importante:

Este proyecto se presenta como una guía para facilitar el aprendizaje y la aplicación práctica de las tecnologías y conceptos vistos en clase. Sin embargo, **se valora y alienta cualquier iniciativa personal que busque mejorar el proyecto o proponer una forma diferente de realizarlo**, siempre que se mantenga el uso de las herramientas y técnicas discutidas durante el curso (Python, Django, SQLite, Plotly, Bootstrap, jQuery).

La única condición es que tu proyecto final incorpore y demuestre comprensión de los contenidos vistos en clase. Estamos abiertos a ver cómo aplicas creativamente tus habilidades para resolver problemas, optimizar funcionalidades o incluso añadir nuevas características que enriquezcan tu proyecto.

